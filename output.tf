output "module_gke-vpc" {
  value = module.gke-vpc
}

output "module_gke" {
  value = module.gke
}

output "cluster_ca_certificate" {
  value = module.gke.cluster_ca_certificate
}

output "cluster_endpoint" {
  value = module.gke.cluster_endpoint
}
